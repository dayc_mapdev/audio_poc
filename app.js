
const connection = new signalR.HubConnectionBuilder()
    // .withUrl('http://localhost:50001/stream')
    .withUrl('http://propipe-test.eastus.cloudapp.azure.com:5005/stream')
    .configureLogging(signalR.LogLevel.Information)
    .build();

connection.start().then(() => {
    console.log('connected to stream');
});

let streamSubject;
document.getElementById('startStream').onclick = (event) => {
    console.log('starting stream...');
    // I called the stream 'Chart' because I used the same method to test streaming charts. I have '11' as a fake UID of a device
    streamSubject = connection.stream('Chart', 11) 
        .subscribe({ 
            next: (item) => {
                console.log(item);
            },
            complete: () => {
                // this stream should never end unless triggered...
                console.log('stream complete');
            },
            error: (err) => {
                console.error(err);
            },
    });
}

document.getElementById('stopStream').onclick = (event) => {
    console.log('stopping stream...');
    if (streamSubject) {
        streamSubject.dispose();
        streamSubject = null;
    }
}
